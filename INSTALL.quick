Prerequisites
=============

MonkeyMedia requires the following packages:

 - A working GNOME 2 platform (more specifically, you need glib-2.0,
   gnome-vfs-2.0, libgnome-2.0, and gconf-2.0.)
   For more information, see:

    http://www.gnome.org/

 - The Ogg Vorbis libraries, available from:

    http://www.vorbis.com/

 - The libid3tag library from the MAD distribution, available from:

    http://www.mars.org/home/rob/proj/mpeg/

 - The musicbrainz library, available from:

    http://www.musicbrainz.org/

 - If you are building the GStreamer backend, you will need:

   - The GStreamer multimedia framework, available from:

      http://www.gstreamer.net/

   - The following GStreamer plugins (you can check their availability with
     gst-inspect <name-of-plugin>):

      * spider (From the GStreamer main distribution)
      * volume (From the gst-plugins cvs module, or gstreamer-audio-effects
        package)
      * gnomevfssrc (From the gst-plugins cvs module, or gstreamer-gnomevfs
        package. Requires gnome-vfs to be installed.)
      * mad (From the gst-plugins cvs module, or gstreamer-mad package.
        Requires mad to be installed (see above for URL). MP3 playback
        will be disabled if this plugin is not available.)
      * vorbis (From the gst-plugins cvs module, or gstreamer-vorbis package.
        Requires the vorbis libraries to be installed (see above for URL).
        Ogg Vorbis playback will be disabled if this plugin is not
        available.)
      * any audiosink (osssink, alsasink, esdsink, to name a few)

      Please note that you probably need to run gst-register after having
      installed the plugins.

 - If you are building the xine backend, you will need:

   - The xine multimedia framework, available in the xine-lib package from:

      http://xine.sourceforge.net/

Simple install procedure
========================

  % gzip -cd monkey-media-0.7.tar.gz | tar xvf - # unpack the sources
  % cd monkey-media-0.7                          # change to the directory
  % ./configure                                  # run the 'configure' script [1]
  % make                                         # build MonkeyMedia
  [ Become root if necessary ]
  % make install                                 # install MonkeyMedia

[1] Pass --enable-xine to configure if you prefer to use the xine instead of the
    GStreamer backend.

The Details
===========

More detailed installation instructions can be found in INSTALL.GNU.
